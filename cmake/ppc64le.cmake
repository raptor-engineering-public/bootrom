################################################################################
###
### @file       ppc64le.cmake
###
### @project
###
### @brief      PPC64LE specific configurations
###
################################################################################
###
################################################################################
###
### @copyright Copyright (c) 2018-2021, Evan Lojewski
### @cond
###
### All rights reserved.
###
### Redistribution and use in source and binary forms, with or without
### modification, are permitted provided that the following conditions are met:
### 1. Redistributions of source code must retain the above copyright notice,
### this list of conditions and the following disclaimer.
### 2. Redistributions in binary form must reproduce the above copyright notice,
### this list of conditions and the following disclaimer in the documentation
### and/or other materials provided with the distribution.
### 3. Neither the name of the copyright holder nor the
### names of its contributors may be used to endorse or promote products
### derived from this software without specific prior written permission.
###
################################################################################
###
### THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
### AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
### IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
### ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
### LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
### CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
### SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
### INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
### CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
### ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
### POSSIBILITY OF SUCH DAMAGE.
### @endcond
################################################################################

SET(PPC64LE_COMPILE_OPTIONS "-DLINT_FILE=clang-ppc64le"
    -integrated-as
    -nostdlib -nodefaultlibs -nostdinc
    -fomit-frame-pointer
    -fno-builtin
    -flto
    # -include "${CMAKE_SOURCE_DIR}/include/banned.h"
    -mno-vsx -mno-altivec
    -mlittle-endian -fno-stack-protector -mcmodel=small
    -mabi=elfv2
    -target powerpc64le-unknown-linux-gnu -mfloat-abi=soft
    -fexceptions
)

FIND_PROGRAM(PPC64LE_LINK_EXECUTABLE
    NAMES ld.lld
    REQUIRED)

SET(PPC64LE_LINK_OPTIONS --gc-sections -static)
# SET(CMAKE_EXE_LINKER_FLAGS -static)

# ASM files
SET(CMAKE_INCLUDE_FLAG_ASM "-I")
SET(CMAKE_ASM_COMPILE_OBJECT "<CMAKE_ASM_COMPILER> -x assembler-with-cpp <DEFINES> <INCLUDES> <FLAGS> -o <OBJECT> -c <SOURCE>")

SET(CMAKE_ppc64le_LINK_EXECUTABLE "${PPC64LE_LINK_EXECUTABLE} <OBJECTS> <LINK_LIBRARIES> <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> -Bstatic -o <TARGET>")

SET(PPC64LE_CMAKE_DIR ${CMAKE_CURRENT_LIST_DIR})
SET(PPC64LE_LINT_FILE ${CMAKE_BINARY_DIR}/.clang-ppc64le.lnt)

generate_lint_config("${PPC64LE_COMPILE_OPTIONS}" ${PPC64LE_LINT_FILE} ${CMAKE_BINARY_DIR}/.clang-ppc64le.h)

# PPC64LE-specific executables
function(ppc64le_add_executable target)
    add_executable(${target} ${ARGN})

    target_compile_options(${target} PRIVATE ${PPC64LE_COMPILE_OPTIONS})
    target_link_libraries(${target} PRIVATE ${PPC64LE_LINK_OPTIONS})
    set_property(TARGET ${target} PROPERTY LINKER_LANGUAGE ppc64le)

    # add_custom_command(
    #     TARGET ${target} POST_BUILD
    #     COMMAND elf2ape -n ${target} -i ${target} -o ${target}.bin
    #     BYPRODUCTS ${target}.bin
    #     DEPENDS elf2ape)

    add_custom_command(
        TARGET ${target} POST_BUILD
        COMMAND objcopy -O binary ${target} ${target}.bin
        BYPRODUCTS ${target}.bin)

    add_custom_command(
        OUTPUT ${target}.c
        COMMAND bin2c --input ${target}.bin --output ${target}.c
        DEPENDS ${target} bin2c
        VERBATIM)

    SET(RESOURCES
        ${CMAKE_CURRENT_BINARY_DIR}/${target}.bin)
    set_target_properties(${target} PROPERTIES RESOURCE "${RESOURCES}")

    # Add host binary
    add_library(${target}-binary EXCLUDE_FROM_ALL ${target}.c)
endfunction(ppc64le_add_executable)

function(ppc64le_generate_init target)
    # FPGA init files are assumed to end with a CRC.
    add_custom_command(
        OUTPUT ${target}.crcbin
        COMMAND crc32 -i ${target}.bin -o ${target}.crcbin
        DEPENDS ${target} crc32
        )

    add_custom_command(
        OUTPUT ${target}.init
        COMMAND bin2init --input ${target}.crcbin --output ${target}.init
        DEPENDS ${target}.crcbin bin2init
        VERBATIM)

    add_custom_target(${target}-init ALL DEPENDS ${target}.init)

    get_target_property(RESOURCES ${target} RESOURCE)
    LIST(APPEND RESOURCES "${CMAKE_CURRENT_BINARY_DIR}/${target}.init")
    set_target_properties(${target} PROPERTIES RESOURCE "${RESOURCES}")
endfunction(ppc64le_generate_init)


# PPC64LE-specific libraries
function(ppc64le_add_library target)
    add_library(${target} ${ARGN})

    target_compile_options(${target} PRIVATE ${PPC64LE_COMPILE_OPTIONS})
    set_target_properties(${target} PROPERTIES LINT_CONFIG ${PPC64LE_LINT_FILE})
endfunction(ppc64le_add_library)

function(ppc64le_linker_script target script)
    set_property(TARGET ${target} APPEND APPEND_STRING PROPERTY LINK_FLAGS " -L\"${FPGA_BUILD_DIR}/software/include/\" ")
    set_property(TARGET ${target} APPEND APPEND_STRING PROPERTY LINK_FLAGS " --script=\"${script}\" ")

    set_target_properties(${target} PROPERTIES LINK_DEPENDS ${script})
endfunction(ppc64le_linker_script)
