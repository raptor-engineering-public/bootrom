// (c) 2020 Raptor Engineering, LLC <sales@raptorengineering.com>
// (c) 2021 Evan Lojewski

#ifndef __IRQ_H
#define __IRQ_H

#ifdef __cplusplus
extern "C" {
#endif

#include <system.h>
#include <generated/csr.h>
#include <generated/soc.h>
#include <generated/mem.h>

// Address of exception / IRQ handler routine
extern void * __rom_isr_address;
void isr(uint64_t vec);

// External interrupt enable bit
#define PPC_MSR_EE_SHIFT	15

// XICS registers
#define PPC_XICS_XIRR_POLL	0x0
#define PPC_XICS_XIRR		0x4
#define PPC_XICS_RESV		0x8
#define PPC_XICS_MFRR		0xc

// Must match corresponding XICS ICS HDL parameter
#define PPC_XICS_SRC_NUM	16

// Default external interrupt priority set by software during IRQ enable
#define PPC_EXT_INTERRUPT_PRIO	0x08

#define KESTREL_SPR_KAISB    (850) /**< Kestrel architecture-specific Interrupt Vector Base register. */

#define EXCEPTION_MASK          (0xFFF) /**< Maximum valid exception address */
#define EXCEPTION_RESET         (0x100) /**< The causes of system reset exceptions are implementation-dependent. */
#define EXCEPTION_DSI           (0x300) /**< A DSI exception occurs when a data memory access cannot be performed. */
#define EXCEPTION_DSegI         (0x380) /**< A DSegI exception occurs when a data memory access cannot be performed due to a segment fault. */
#define EXCEPTION_ISI           (0x400) /**< An ISI exception occurs when an instruction fetch cannot be performed. */
#define EXCEPTION_ISegI         (0x480) /**< An ISegI exception occurs when an instruction fetch cannot be performed due to a segment fault. */
#define EXCEPTION_EXTERNAL_IRQ  (0x500) /**< An external interrupt is generated only when an external exception is pending and the interrupt is enabled (MSR[EE] = 1). */
#define EXCEPTION_PROGRAM       (0x700) /**< A program exception is caused by conditions which correspond to bit settings in SRR1 and arise during execution of an instruction. */
#define EXCEPTION_DECREMENTER   (0x900) /**< The exception is created when the most significant bit changes from 0 to 1. */
#define EXCEPTION_SYSTEM_CALL   (0xC00) /**< A system call exception occurs when a System Call (sc) instruction is executed. **/

#define bswap32(x) (uint32_t)__builtin_bswap32((uint32_t)(x))

/**
 * Update the interupt vector table base address.
 *
 * @param ivt_base The 16K aligned interrupt vector table base address.
 */
static inline void irq_set_base(void *ivt_base)
{
	__asm__ volatile("mtspr %0, %1" : : "i" (KESTREL_SPR_KAISB), "r" (ivt_base) : "memory");
}

/**
 * Read the interupt vector table base address.
 *
 * @returns The 16K aligned interrupt vector table base address.
 */
static inline void *irq_get_base(void)
{
    void* ivt_base;
	__asm__ volatile("mfspr %0, %1" : "=r"(ivt_base) : "i" (KESTREL_SPR_KAISB) : "memory");
    return ivt_base;
}

static inline uint8_t xics_icp_readb(int reg)
{
	return *((uint8_t*)(XICSICP_BASE + reg));
}

static inline void xics_icp_writeb(int reg, uint8_t value)
{
	*((uint8_t*)(XICSICP_BASE + reg)) = value;
}

static inline uint32_t xics_icp_readw(int reg)
{
	return bswap32(*((uint32_t*)(XICSICP_BASE + reg)));
}

static inline void xics_icp_writew(int reg, uint32_t value)
{
	*((uint32_t*)(XICSICP_BASE + reg)) = bswap32(value);
}

static inline uint32_t xics_ics_read_xive(int irq_number)
{
	return bswap32(*((uint32_t*)(XICSICS_BASE + 0x800 + (irq_number << 2))));
}

static inline void xics_ics_write_xive(int irq_number, uint32_t priority)
{
	*((uint32_t*)(XICSICS_BASE + 0x800 + (irq_number << 2))) = bswap32(priority);
}

static inline void mtmsrd(uint64_t val)
{
	__asm__ volatile("mtmsrd %0" : : "r" (val) : "memory");
}

static inline uint64_t mfmsr(void)
{
	uint64_t rval;
	__asm__ volatile("mfmsr %0" : "=r" (rval) : : "memory");
	return rval;
}

static inline void mtdec(uint64_t val)
{
	__asm__ volatile("mtdec %0" : : "r" (val) : "memory");
}

static inline uint64_t mfdec(void)
{
	uint64_t rval;
	__asm__ volatile("mfdec %0" : "=r" (rval) : : "memory");
	return rval;
}

static inline unsigned int irq_getie(void)
{
	return (mfmsr() & (1 << PPC_MSR_EE_SHIFT)) != 0;
}

static inline void irq_setie(unsigned int ie)
{
	if (ie)
	{
		// Unmask all IRQs
		xics_icp_writeb(PPC_XICS_XIRR, 0xff);

		// Enable DEC + external interrupts
		mtmsrd(mfmsr() | (1 << PPC_MSR_EE_SHIFT));
	}
	else
	{
		// Disable DEC + external interrupts
		mtmsrd(mfmsr() & ~(1 << PPC_MSR_EE_SHIFT));

		// Mask all IRQs
		xics_icp_writeb(PPC_XICS_XIRR, 0x00);
	}
}

static inline unsigned int irq_getmask(void)
{
	// Compute mask from enabled external interrupts in ICS
	uint32_t mask;
	int irq;
	mask = 0;
	for (irq = PPC_XICS_SRC_NUM - 1; irq >= 0; irq--) {
		mask = mask << 1;
		if ((xics_ics_read_xive(irq) & 0xff) != 0xff)
			mask |= 0x1;
	}
	return mask;
}

static inline void irq_setmask(unsigned int mask)
{
	int irq;

	// Enable all interrupts at a fixed priority level for now
	int priority_level = PPC_EXT_INTERRUPT_PRIO;

	// Iterate over IRQs configured in mask, and enable / mask in ICS
	for (irq = 0; irq < PPC_XICS_SRC_NUM; irq++) {
		if ((mask >> irq) & 0x1)
			xics_ics_write_xive(irq, priority_level);
		else
			xics_ics_write_xive(irq, 0xff);
	}
}

static inline unsigned int irq_pending(void)
{
	// Compute pending interrupt bitmask from asserted external interrupts in ICS
	uint32_t pending;
	int irq;
	pending = 0;
	for (irq = PPC_XICS_SRC_NUM - 1; irq >= 0; irq--) {
		pending = pending << 1;
		if ((xics_ics_read_xive(irq) & (0x1 << 31)) != 0)
			pending |= 0x1;
	}
	return pending;
}

#ifdef __cplusplus
}
#endif

#endif /* __IRQ_H */
