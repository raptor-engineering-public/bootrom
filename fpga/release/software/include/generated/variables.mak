TRIPLE=powerpc64le-linux-gnu
CPU=microwatt
CPUFLAGS=-m64 -mabi=elfv2 -msoft-float -mno-string -mno-multiple -mno-vsx -mno-altivec -mlittle-endian -mstrict-align -fno-stack-protector -mcmodel=small -D__microwatt__ 
CPUENDIANNESS=little
CLANG=0
CPU_DIRECTORY=/home/meklort/litex-repos/litex/litex/soc/cores/cpu/microwatt
COMPILER_RT_DIRECTORY=/home/meklort/litex-repos/pythondata-software-compiler_rt/pythondata_software_compiler_rt/data
SOC_DIRECTORY=/home/meklort/litex-repos/litex/litex/soc
export BUILDINC_DIRECTORY
BUILDINC_DIRECTORY=/home/meklort/litex-repos/litex-boards/litex_boards/targets/build/versa_ecp5/software/include
LIBCOMPILER_RT_DIRECTORY=/home/meklort/litex-repos/litex/litex/soc/software/libcompiler_rt
LIBBASE_DIRECTORY=/home/meklort/litex-repos/litex/litex/soc/software/libbase
LIBLITEDRAM_DIRECTORY=/home/meklort/litex-repos/litex/litex/soc/software/liblitedram
LIBLITEETH_DIRECTORY=/home/meklort/litex-repos/litex/litex/soc/software/libliteeth
LIBLITESPI_DIRECTORY=/home/meklort/litex-repos/litex/litex/soc/software/liblitespi
LIBLITESDCARD_DIRECTORY=/home/meklort/litex-repos/litex/litex/soc/software/liblitesdcard
BIOS_DIRECTORY=/home/meklort/litex-repos/litex/litex/soc/software/bios
