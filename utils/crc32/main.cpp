////////////////////////////////////////////////////////////////////////////////
///
/// @file       main.cpp
///
/// @project
///
/// @brief      Utility to stuff crc32 value into the bootrom.
///
////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////
///
/// @copyright Copyright (c) 2021, Evan Lojewski
/// @cond
///
/// All rights reserved.
///
/// Redistribution and use in source and binary forms, with or without
/// modification, are permitted provided that the following conditions are met:
/// 1. Redistributions of source code must retain the above copyright notice,
/// this list of conditions and the following disclaimer.
/// 2. Redistributions in binary form must reproduce the above copyright notice,
/// this list of conditions and the following disclaimer in the documentation
/// and/or other materials provided with the distribution.
/// 3. Neither the name of the copyright holder nor the
/// names of its contributors may be used to endorse or promote products
/// derived from this software without specific prior written permission.
///
////////////////////////////////////////////////////////////////////////////////
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
/// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
/// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
/// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
/// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
/// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
/// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
/// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
/// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
/// POSSIBILITY OF SUCH DAMAGE.
/// @endcond
////////////////////////////////////////////////////////////////////////////////

#include "crc.h"

#include <OptionParser.h>
#include <algorithm>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <stdint.h>

#define _STRINGIFY(__STR__) #__STR__
#define STRINGIFY(__STR__)  _STRINGIFY(__STR__)

#define VERSION_STRING STRINGIFY(VERSION_MAJOR) "." STRINGIFY(VERSION_MINOR) "." STRINGIFY(VERSION_PATCH)

using namespace std;
using optparse::OptionParser;

int main(int argc, char const *argv[])
{
    uint32_t crc;
    ofstream outfile;
    ifstream infile;
    OptionParser parser = OptionParser().description("Binary to C Header utility v" VERSION_STRING);

    parser.version(VERSION_STRING);

    parser
        .add_option("-i", "--input") // Force clang-format to add breaks
        .dest("input")
        .help("Input binary file")
        .metavar("INPUT");

    parser
        .add_option("-o", "--output") // Force clang-format to add breaks
        .dest("output")
        .help("Output binary file with stuffed crc")
        .metavar("OUTPUT");

    optparse::Values options = parser.parse_args(argc, argv);
    vector<string> args = parser.args();

    if (!options.is_set("input"))
    {
        parser.error("Please specify an input binary file to use.");
    }

    if (!options.is_set("output"))
    {
        parser.error("Please specify an output binary file to write.");
    }

    string filename = options["input"];

    infile.open(options["input"], ios::in | ios::binary);
    if (!infile.is_open())
    {
        parser.error("Unable to open input file " + options["input"]);
    }

    outfile.open(options["output"], ios::out | ios::trunc | ios::binary);
    if (!outfile.is_open())
    {
        parser.error("Unable to open output file " + options["output"]);
    }

    // get pointer to associated buffer object
    filebuf *pbuf = infile.rdbuf();

    // get file size using buffer's members
    size_t size = pbuf->pubseekoff(0, infile.end, infile.in);
    pbuf->pubseekpos(0, infile.in);

    if (size <= sizeof(crc))
    {
        parser.error("Input file size must be larger than 4 bytes.");
    }

    // allocate memory to contain file data
    char *buffer = new char[size];

    // get file data
    pbuf->sgetn(buffer, size);

    // Calculate the CRC over the file, minus 4 bytes - last 4 bytes store the crc.
    crc = crc32((unsigned char *)buffer, size - sizeof(crc));

    memcpy(&buffer[size - sizeof(crc)], &crc, sizeof(crc));

    outfile.write(buffer, size);
    outfile.close();

    delete[] buffer;

    return 0;
}
