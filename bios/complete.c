// This file is Copyright (c) 2020 Franck Jullien <franck.jullien@gmail.com>
//
//     Largely inspired/copied from U-boot and Barebox projects wich are:
//         Sascha Hauer, Pengutronix, <s.hauer@pengutronix.de>

// License: BSD

#include "complete.h"

#include "command.h"
#include "helpers.h"
#include "readline.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char *prev_outstr = NULL;

static int string_min_common(const char *prefix, const char *instr)
{
    int max_check = strlen(instr);
    int min_common = max_check;

    struct command_struct *const *cmd;

    for (cmd = __bios_cmd_start; cmd != __bios_cmd_end; cmd++)
    {
        const char *prefix_check = prefix;
        int skip = 0;
        int i;

        for (i = 0; i < max_check; i++)
        {
            // Skip any prefix characters
            if (*prefix_check)
            {
                // Skip leading prefix chars
                if (*prefix_check != (*cmd)->name[i])
                {
                    // Skip this command.
                    skip = 1;
                    break;
                }

                prefix_check++;
            }

            if (instr[i] != (*cmd)->name[i])
            {
                break;
            }
        }

        if (!skip && i < min_common)
        {
            min_common = i;
        }
    }

    return min_common;
}

static int string_max_common(const char *instr, const char **common_str)
{
    int max_check = strlen(instr);
    int max_common = 0;

    struct command_struct *const *cmd;

    for (cmd = __bios_cmd_start; cmd != __bios_cmd_end; cmd++)
    {
        int i;

        for (i = 0; i < max_check; i++)
        {
            if (instr[i] != (*cmd)->name[i])
            {
                break;
            }
        }

        if (i > max_common)
        {
            if (common_str)
            {
                *common_str = (*cmd)->name;
            }
            max_common = i;
        }
    }

    return max_common;
}

// int string_num_common(char *)
static const char *string_skip_common(char *instr, int *new_chars)
{
    const char *common_str = NULL;
    int max_check = strlen(instr);
    (void)string_max_common(instr, &common_str);

    // Find the min number of common characters for the common_str;
    int actual_common = string_min_common(instr, common_str);

    if (actual_common > max_check)
    {
        *new_chars = actual_common - max_check;
    }

    return &common_str[max_check];
}

static const char *string_list_entry(char *instr, int index)
{
    int count = 0;
    struct command_struct *const *cmd;

    for (cmd = __bios_cmd_start; cmd != __bios_cmd_end; cmd++)
    {
        if (!strncmp(instr, (*cmd)->name, strlen(instr)))
        {
            if (count == index)
            {
                return (*cmd)->name;
            }
            count++;
        }
    }
    return NULL;
}

static void string_list_print_by_column(char *instr)
{
    int len = 0, num, i;
    const char *entry;
    i = 0;
    while (NULL != (entry = string_list_entry(instr, i++)))
    {
        int l = strlen(entry) + 4;
        if (l > len)
        {
            len = l;
        }
    }

    num = 80 / (len + 1);

    i = 0;
    while (NULL != (entry = string_list_entry(instr, i++)))
    {
        printf("%-*s", len, entry);
        if (!((i + 1) % num))
        {
            printf("\n");
        }
    }

    if (i % num)
    {
        printf("\n");
    }
}

int complete(char *instr, const char **outstr, int *num_chars)
{
    int reprint = 0;

    if (!instr)
    {
        // Reset internal state.
        prev_outstr = NULL;
        return 0;
    }

    *outstr = string_skip_common(instr, num_chars);
    if (string_list_entry(instr, 1))
    {
        // Skip as many characters as possible
        if (prev_outstr != *outstr + *num_chars)
        {
            printf("\n");
            string_list_print_by_column(instr);
            reprint = 1;
            prev_outstr = *outstr + *num_chars;
        }
    }

    return reprint;
}
